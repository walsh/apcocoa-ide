-------------------------------------------------------
-------------------------------------------------------
-- >>> SHORT INTRODUCTION TO CoCoA-5/ApCoCoA-2  <<< ---
-------------------------------------------------------
-------------------------------------------------------
--
-- Interactive system CoCoA-5 is built on top of C++ software
-- library CoCoALib.  CoCoA-5 is an application which uses
-- CoCoALib for performing the mathematical computations.
--
-- The name comes from Co-mputational Co-mmutative A-lgebra
--                     Co-            Co-          A
--            Ap-plied Co-mputational Co-mmutative A-lgebra
--            Ap-      Co-            Co-          A
-- Homepage:
--           http://apcocoa.uni-passau.de
--           http://cocoa.dima.unige.it/
--
-- Use interfaces with ApCoCoA (or Emacs or Vim)

-------------------------------------------------------
-----------<<< Starting CoCoA/ApCoCoA >>>--------------
-- New Project/File:
-- File> New/Visit New File > Project/File
-- The extension of the files/packages are:
--     *.cocoa5 or *.cpkg5

-------------------------------------------------------
-----------<<< Editor on CoCoA/ApCoCoA >>>-------------
--1. Shortcuts: depends on ApCoCoA (or Emacs)

--2. Send to CoCoA (sends your code from the editor
--    to the CoCoA process and starts the computation):
--    with ApCoCoA (or Emacs): Ctrl+Enter (Alt+Q)

--3. Comments
   -- The sequence minus-minus introduces an end-of-line comment.
   // The sequence slash-slash introduces an end-of-line comment.
   --/* slash-star starts a comment; star-slash ends it */
--4. Get Info about CoCoA-built-in functions
-- Search using QUESTION-MARK followed by keyword

?factor

-- DOUBLE QUESTION-MARK lists the title of manual pages which
-- contain the keyword you searched for:

??factor

-- For newcomers:
?tutorial  --> Tutorials (still incomplete)
?syntax    --> index to all CoCoA-5 commands



-------------------------------------------------------
-- First Step in Computations 
-------------------------------------------------------
-- Tell CoCoA which ring you want to use: avoids ambiguity!
-- Note the DOUBLE-COLON!

use QQx ::= QQ[x];       --> coefficients in the rationals
factor(x^2-2);

use FF7x ::= ZZ/(7)[x];  --> coeffs in finite field of 7 elements
factor(x^2-2);

-- You can store a ring in a variable (recommended):
QQxy ::= QQ[x,y];
use QQxy;
(x+2*y)^2;
f := It;  -->  "It" refers to last unassigned result
factor(f-4);

-------------------------------------------------------
-- Basic Calculation with Numbers
-------------------------------------------------------
1+2+3-4;
2022*5;
2022/3;
mod(10,3);
--1
div(10,3);
--3

-- IsPrime versus IsProbPrime:
-- ** IsProbPrime: fast, but may give wrong answer;
-- ** IsPrime: always correct, but may be slow.
N := 179147518278221287799;
IsProbPrime(N);  --> instant!
IsPrime(N);      --> slower!

-- All numbers are **EXACT** integers or rationals;
-- no floating-point numbers ((except twin-float)).

PrintLn "2^100 = ", 2^100;

factorial(1234);  --> no size limit for integers
FloatStr(It);     --> as a string, easier to understand

pi := 3.14159265;  --> converted into a rational!
PrintLn "pi is approximately ", pi;

-- Beware of big answers:
A := sum([1/n | n in 1..10000]);
A > 9.7;
A < 9.8;
FloatStr(A);
PrintLn "True value is ", A; --> many digits

-- Factorization in the ring of integers
Use Z ::= ZZ;
a := RingElem(Z, 2022);
factor(a);


-------------------------------------------------------
-- LISTS (are very common and useful objects)
-------------------------------------------------------
-- CoCoA-5 has a flexible list data-structure.
-- Lists are printed as sequences of values inside square brackets.
-- The entries are indexed starting from 1 (not from 0 as in C++).
--
-- Square brackets around list elements: [2, 3, 5]
-- Square brackets for indices:          L[1]
-- Round brackets for function calls:    len(L)
--
-- Relevant manual entries:
?list
--
?list constructors
--
10..20;             --> consecutive integer range
--
L := [11, 22, 33];  --> create explicit list
--
L[1] + L[2];        --> index elements of a list
L[2] := 13;         --> change a list entry
len(L);             --> length of the list
--
[n in L | IsPrime(n)]; --> new list containing primes in L
--
append(ref L,2023); --> to append an object "2022" to a list L.
L;
--
remove(ref L,3);  --> to remove an object from a list L
L;
--
LL := [1,3,5];
concat(L,LL);  --> to combine two lists L and LL into one.
-- Other basic functions: First(L,N), Last(L,N), Diff(L,LL)
-- Not possible :(
[i*j | i in 1..5 and j in 1..5];
-- instead do this:
[i[1]*i[2] | i in 1..5 >< 1..5];
1..5 >< 1..5; -- cartesian product of lists
[product(i) | i in 1..5 >< 1..5];
-- Other structures:
-- [< Object > |X In < List >]
-- [< Object > |X In < LIST > And < logicalcondition >]
-- [X In < List > | < logicalcondition >]


-------------------------------------------------------
-- MATRICES
-------------------------------------------------------
-- CoCoA-5 can perform various operations on matrices:
-- Search the manual for a complete list: ??mat

M :=    mat(    [[1,2], [3,4]]);  // By default entries are in QQ
M_ZZ := mat(ZZ, [[1,2], [3,4]]);  // Like M but entries are in ZZ
println M;
M_ZZ;
println M^2 - 5*M;
--
-- Inspector functions
RingOf(M);  --> ring containing the entries
det(M);     --> determinant
inverse(M); --> inverse
M[2,1];     --> access matrix entry
--
-- Hilbert matrix - no problem with "ill-conditioning"
H10 := matrix([[1/(i+j-1) | j in 1..10] | i in 1..10]);
println H10;
det(H10);
--
H100 := matrix([[1/(i+j-1) | j in 1..100] | i in 1..100]);
D100 := det(H100);
FloatStr(D100);
--
-- We can solve a linear system
X := LinSolve(H10, ColMat(1..10));
X;


-------------------------------------------------------
--  Basic Calculation with Polynomials
-------------------------------------------------------
-- Several functions to "deconstruct" a polynomial:
use QQxy ::= QQ[x,y];
f := x^2*y +2*x*y^2 +3*y^2-4;
g_1 := x*y-1;
g_2 :=  y^2-1;
L := [g_1, g_2];
LC(f);  --> leading coefficient
LM(f);  --> leading monomial
LT(f);  --> leading term (or leading power-product)
DF(f);  --> leading form
--
coefficients(f);  --> all coefficients
support(f);       --> all power-products
monomials(f);     --> all monomials
--
g_1+g_2;
g_1*g_2;
--g_1/g_2; --> inexact division
DivAlg(f, L); --> Division f by L with remainder
NR(f,L); --> get remainder


-------------------------------------------------------
-- More things about Polynomials 
-------------------------------------------------------
-- Specify the term-ordering when you create the ring:
P     ::= QQ[x,y,z];     --> DegRevLex is default order
P_lex ::= QQ[x,y,z],lex; --> choose lex order (with x > y > z)
P = P_lex;  --> rings are NOT EQUAL!
  
use P;
L1 := [x^3 -y*z^2 -z^3,  y^3 -x*y*z +x*z^2,  z^3 -x^2 +z^2];
I1 := ideal(L1);
GBasis(I1);     --> compute a Gr?bner basis of I1
--
use P_lex;
L2 := [x^3 -y*z^2 -z^3,  y^3 -x*y*z +x*z^2,  z^3 -x^2 +z^2];
I2 := ideal(L2);  --> "same as" ideal I1
GBasis(I2);     --> compute a Gr?bner basis of I2
--
I1 = I2;        --> ERROR!  They belong to different rings!
RingOf(I1);     --> ring of the ideal I1 = P
RingOf(I2);     --> ring of the ideal I2 = P_lex
L1 = L2;        --> ERROR!  They belong to different rings!
L1[1] = L2[1];  --> ERROR!  They belong to different rings!

-- Get term-ordering as a matrix:
OrdMat(CurrentRing);
OrdMat(P);
--
-- An example with a bigger G-basis:
-- use QQ[x,y,z,t],lex;
-- I := ideal(4*y^2*t^2 +8*y*z^3 +y*t,
--           5*x^3 -8*x^2 -9*x*t,
--           5*x^2*z^2 -8*x*y^2 -2*z*t^3);
-- RGB := ReducedGBasis(I);
-- indent(RGB); --> a long list of polynomials


-------------------------------------------------------
-- RECORDS: data-structures with named fields
-------------------------------------------------------
-- CoCoA-5 offers the data-structure RECORD for bringing
-- together values, each one with its own label.
-- The names of the labels are chosen to make it easy to
-- comprehend.  Each label-value pair is called a "field".
-- The value of a field can be accessed via the dot operator.
--
-- Relevant manual entry:
?record
-- For example: factor returns a RECORD
use QQx ::=QQ[x];
facs := factor(x^4-x^2); // factorize a polynomial
indent(facs); // "indent" prints out more readably
// record[
//   RemainingFactor := 1,
//   factors := [x +1,  x -1,  x],
//   multiplicities := [1,  1,  2]
// ]
facs.factors;  --> dot operator to access a field
//  [x +1,  x -1,  x]


-------------------------------------------------------
-- PROGRAM FLOW CONTROL
-------------------------------------------------------
-- For-loop:
-- for i:=1 to 100 do
--     <commands>
-- endfor;
for i:=1 to 10 do
  println i^3;
endfor;

-- Foreach-loop:
-- foreach x in L do
--     <commands>
-- endforeach;

-- While-loop:
-- while <logical condition> do
--     <commands>
-- endwhile;

-- Repeat-loop:
-- repeat
--     <commands>
-- until(condition);

-- If-function: 
-- if <logical condition> then <Commands> endif;
-- if <logical condition> then <Cmds1> else <Cmds2> endif;


-------------------------------------------------------
-- DEFINING NEW FUNCTIONS 
-------------------------------------------------------
-- Relevant manual entries:
-->   ?define
-->   ?return

-- Sum of squares A and B:
define SumOfSquares(A, B)
  return A^2 + B^2;
enddefine; -- SumOfSquares
-- apply the function:
SumOfSquares(4,3);
--

-- The Euclidean Algorithm: compute gcd(A,B)
Define Euclide(A,B)
  A := Abs(A); B := Abs(B);
  If A=0 And B=0 Then Return 0; EndIf;
  If A=0 And B<>0 Then Return B; EndIf;
  If A<>0 And B=0 Then Return A; EndIf;
  If A<>0 And B<>0 Then 
    If A>B Then L := [B,A]; Else L := [A,B]; EndIf;
    E:=1;
    While E <> 0 Do
      Q := Div(L[2],L[1]); -- L[2]=Q*L[1]+E, E<L[1]
      E := L[2]-Q*L[1];
      L := [E, L[1]];
    EndWhile;
    Return L[2];
  EndIf;
EndDefine; -- Euclide
-- apply the function 
Euclide(2^(17), 100^5);
--1024 


-------------------------------------------------------
-- FURTHER EXAMPLES 
-------------------------------------------------------
--   Making an algebraic extension
--
P ::= QQ[sqrt2, sqrt3];
use P;
I := ideal(sqrt2^2 - 2,  sqrt3^2 - 3);
K := P/I;  --> alg extn generated by sqrt2 and sqrt3
use R ::= K[x,y]; --> polys in x & y with coeffs in k
1/(sqrt2+sqrt3);
It*(sqrt2+sqrt3);
(x+sqrt2+sqrt3)^2;
factor(It);

-------------------------------------------------------
--   Solving (0-dim) polynomial system
--
use QQxy ::= QQ[x,y];
sys := [x^2 + y^2 - 5^2,  3*x + 4*y];
PrintLn "Rational solutions of the polynomial system ", sys;
RationalSolve(sys);
--
sys := [x^2 + y^2 - 50,  3*x + 4*y];
RationalSolve(sys); --> no rational solutions
ApproxSolns := ApproxSolve(sys);   --> approx real solutions
PrintLn "Approx solutions of the polynomial system ", sys;
indent(ApproxSolns);
-- use FloatStr or DecimalStr to make result more readable:
indent([[ FloatStr(coord) | coord in soln]|
	soln in ApproxSolns.AffinePts]);

-------------------------------------------------------
--  Using Elimination function
-->   ? elim
use R ::= QQ[t,x,y,z];
I := ideal(x-t^3, y-t^5, z-t^15-t^6-t);
-- compute the intersection I and Q[x,y,z]:
E1 := elim(t, I);
indent(E1);
-- compute the intersection I and Q[x,y]:
E2 := elim([t,z],I);
indent(E2);


-------------------------------------------------------
--  Using a package borderbasis with lias BB
use P ::= QQ[x,y];
OO := [one(P),x,y,x*y];
BB.IsOrderIdeal(OO);  --> check if OO is an order ideal
BB.Border(OO);        --> compute the border of OO

-------------------------------------------------------
-------------------------------------------------------
-------------------------------------------------------
