#!/bin/bash

read -p "username: " username

echo "move old_files"
ssh -q -T $username@apcocoa.uni-passau.de > /dev/null <<  HERE
cd public_html/download
mv apcocoa2_unix.tar.xz apcocoa2_unix_old.tar.xz
mv apcocoa2_win.zip apcocoa2_win_old.zip
HERE

echo "upload unix-archive"
scp apcocoa2_unix.tar.xz $username@apcocoa.uni-passau.de:/home/apcocoa/public_html/download/
echo "upload win-archive"
scp apcocoa2_win.zip $username@apcocoa.uni-passau.de:/home/apcocoa/public_html/download

echo "done!"
exit 0
