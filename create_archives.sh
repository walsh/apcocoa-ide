#!/bin/bash

git submodule update --init --recursive
git submodule update --init --remote

echo "creating linux archive"
tar cf - apcocoa2_unix | xz -z -0 - > apcocoa2_unix.tar.xz

echo "creating windows archive"
zip -r apcocoa2_win.zip apcocoa2_win

